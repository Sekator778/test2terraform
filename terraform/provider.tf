terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.18.0"
    }
  }

  backend "http" {
    address = "https://gitlab.com/api/v4/projects/46072404/terraform/state/default"
    lock_address = "https://gitlab.com/api/v4/projects/46072404/terraform/state/default/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/46072404/terraform/state/default/lock"
  }
}

provider "aws" {
  region = "eu-central-1"
}
